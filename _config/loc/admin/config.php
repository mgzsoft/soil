<?php
// HTTP
define('HTTP_SERVER', 'http://ocart/admin/');
define('HTTP_CATALOG', 'http://ocart/');

// HTTPS
define('HTTPS_SERVER', 'http://ocart/admin/');
define('HTTPS_CATALOG', 'http://ocart/');

// DIR
define('DIR_APPLICATION', 'C:/OpenServer/OSPanel/domains/ocart/admin/');
define('DIR_SYSTEM', 'C:/OpenServer/OSPanel/domains/ocart/system/');
define('DIR_IMAGE', 'C:/OpenServer/OSPanel/domains/ocart/image/');
define('DIR_LANGUAGE', 'C:/OpenServer/OSPanel/domains/ocart/admin/language/');
define('DIR_TEMPLATE', 'C:/OpenServer/OSPanel/domains/ocart/admin/view/template/');
define('DIR_CONFIG', 'C:/OpenServer/OSPanel/domains/ocart/system/config/');
define('DIR_CACHE', 'C:/OpenServer/OSPanel/domains/ocart/system/storage/cache/');
define('DIR_DOWNLOAD', 'C:/OpenServer/OSPanel/domains/ocart/system/storage/download/');
define('DIR_LOGS', 'C:/OpenServer/OSPanel/domains/ocart/system/storage/logs/');
define('DIR_MODIFICATION', 'C:/OpenServer/OSPanel/domains/ocart/system/storage/modification/');
define('DIR_UPLOAD', 'C:/OpenServer/OSPanel/domains/ocart/system/storage/upload/');
define('DIR_CATALOG', 'C:/OpenServer/OSPanel/domains/ocart/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'ocart');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
