<?php
// Heading
$_['heading_title']    = 'Личный кабинет';

// Новые пункты
$_['text_settlements'] = 'Взаиморасчеты';
$_['text_real']        = 'Реализация';
$_['text_delivery']    = 'График доставок';

$_['text_account']     = 'Личный кабинет';
$_['text_order']       = 'Заказы';
$_['text_password']    = 'Смена пароля';

$_['text_register']    = 'Регистрация';
$_['text_login']       = 'Авторизация';
$_['text_logout']      = 'Выйти';
$_['text_forgotten']   = 'Напомнить пароль';
$_['text_edit']        = 'Учетная запись';
$_['text_address']     = 'Адреса доставки';
$_['text_wishlist']    = 'Мои закладки';
$_['text_download']    = 'Файлы для скачивания';
$_['text_reward']      = 'Бонусные баллы';
$_['text_return']      = 'Возврат товара';
$_['text_transaction'] = 'История платежей';
$_['text_newsletter']  = 'Подписка на новости';
$_['text_recurring']   = 'Периодические платежи';