<?php

class ControllerAccountReal extends Controller
{

    public function index()
    {
        if (!$this->customer->isLogged()) {
            $this->session->data['redirect'] = $this->url->link('account/real', '', true);
            $this->response->redirect($this->url->link('account/login', '', true));
            return;
        }

        $this->load->language('account/real');
        
        $this->load->model('account/one_si');

        $this->document->setTitle($this->language->get('heading_title'));

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_account'),
            'href' => $this->url->link('account/account', '', true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('account/real')
        );

        $data['heading_title'] = $this->language->get('heading_title');

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');
        $data['continue'] = $this->url->link('account/account', '', true);

        $data['real'] = $this->model_account_one_si->getReal($this->customer->getCode());
        $data['real_sum'] = $this->model_account_one_si->getRealSum($data['real']);

        $this->response->setOutput($this->load->view('account/real', $data));
    }

}
