<?php

class ControllerAccountSettlements extends Controller
{
    private function validateDates($date_begin, $date_end)
    {
        // даты приходят с виджета выбора дат в формате 'd-m-Y'
        // проверяем на соответствие формату, если ок, проверяем дата_нач < дата_кон
        // преобразуем в формат 'Ymd' для запроса в 1с
        $obj_date_begin = date_create_from_format ('d-m-Y', $date_begin);
        if ($obj_date_begin == false) {
            return false;
        }

        $obj_date_end = date_create_from_format ('d-m-Y', $date_end);
        if ($obj_date_end == false) {
            return false;
        }
        
        if ($obj_date_begin > $obj_date_end) {
            return false;
        }
        
        $res = Array();
        $res['filter_date_start'] = date_format($obj_date_begin, 'Ymd');
        $res['filter_date_end'] = date_format($obj_date_end, 'Ymd');
        
        return $res;
    }
    
    private function getDocLink($doc)
    {
        // преобразуем в формат 'Ymd' для последующего запроса в 1с
        $doc_date = date_format(date_create($doc['DokumentData']), 'Ymd');
        
        // запятые из 1с в численных данных, php при явном преобразовании
        // во float съедает зпт, явно заменять на точки в строках
        // позже перенести во все числа, получаемые  из 1с
        $doc_prih = str_replace(',', '.', $doc['SummaVzaimoraschetovPrihod']);
        $doc_rash = str_replace(',', '.', $doc['SummaVzaimoraschetovRashod']);
        $doc_sum = (float)$doc_prih + (float)$doc_rash;

        $params = array(
            'dt' => $doc['DocTypeCode'],
            'dn' => $doc['DokumentNomer'],
            'dd' => $doc_date,
            'ds' => urlencode($doc_sum)
        );
        return $this->url->link('account/settlements/doc', $params, true);
    }

    public function index()
    {
        if (!$this->customer->isLogged()) {
            $this->session->data['redirect'] = $this->url->link('account/settlements', '', true);
            $this->response->redirect($this->url->link('account/login', '', true));
            return;
        }
            
        $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
        $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/locale/'.$this->session->data['language'].'.js');
        $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
        $this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

        $this->document->addScript('catalog/view/javascript/soil/soil.js');
        
        $this->load->language('account/settlements');
        $this->load->model('account/one_si');

        // Если еще не нажимали "Сформировать"
        if (empty($this->request->get['filter_date_start'])) {
            $data['filter_date_start'] = date('d-m-Y', mktime(0, 0, 0, date('m')-1, 1, date('Y')));

            $data['filter_date_end'] = date('d-m-Y', time());
            
            $data['report_status'] = 'not_runned';
            $data['docs'] = array();
        }
        else {
            // проверка дат, отрицательный результат, статус отчета
            $report_dates = $this->validateDates(
                $this->request->get['filter_date_start'],
                $this->request->get['filter_date_end']
            );
            
            if ($report_dates == false) {
                $data['report_status'] = 'incorrect_date';
                $data['filter_date_start'] = date('d-m-Y', mktime(0, 0, 0, date('m'), 1, date('Y')));
                $data['filter_date_end'] = date('d-m-Y', time());
                $data['docs'] = array();
            }
            else {
                $data['docs'] = $this->model_account_one_si->getSettlements(
                    $this->customer->getCode(),
                    $report_dates['filter_date_start'], 
                    $report_dates['filter_date_end']
                );

                // во вьюху даты в прежнем формате
                $data['filter_date_start'] = $this->request->get['filter_date_start'];
                $data['filter_date_end'] = $this->request->get['filter_date_end'];

                // Нет доков за выбранный период
                if (!sizeof($data['docs'])) {
                    $data['report_status'] = 'no_result';
                }
                else {
                    // проставляем в доках ссылку на станицу отдельного дока
                    foreach ($data['docs'] as $key => $doc) {
                        $data['docs'][$key]['url'] = $this->getDocLink($doc);
                    }

                    $data['report_status'] = 'ok';
                    $data['balance_begin'] = $data['docs'][0]['SummaVzaimoraschetovNachalyniyOstatok'];
                    $data['balance_end'] = $data['docs'][sizeof($data['docs'])-1]['SummaVzaimoraschetovKonechniyOstatok'];
                }
            }
        }

        $this->document->setTitle($this->language->get('heading_title'));

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_account'),
            'href' => $this->url->link('account/account', '', true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('account/settlements')
        );

        $data['heading_title'] = $this->language->get('heading_title');
        $data['button_continue'] = $this->language->get('button_continue');

        $data['form_url'] = $this->url->link('account/settlements', '', true);
        $data['continue'] = $this->url->link('account/account', '', true);

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');
        
//        // поверить коннект к 1с
//        $this->model_account_one_si->testConnect();
//        return;

        $this->response->setOutput($this->load->view('account/settlements', $data));
    }
    
    public function doc()
    {
        if (!$this->customer->isLogged()) {
            $this->session->data['redirect'] = $this->url->link('account/settlements/doc', '', true);
            $this->response->redirect($this->url->link('account/login', '', true));
            return;        
        }
            
        $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
        $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/locale/'.$this->session->data['language'].'.js');
        $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
        $this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

        $this->document->addScript('catalog/view/javascript/soil/soil.js');
        
        $this->load->language('account/settlements');
        $this->load->model('account/one_si');
        
        // номер, дата и тип дока формируются еще в журнале при формировании url 
        // и содержатся в get запросе
        $doc_info = array();
        $doc_info['doc_num'] = $this->request->get['dn'];
        $doc_info['doc_date'] = $this->request->get['dd'];
        $doc_info['doc_type'] = $this->request->get['dt'];
        $doc_info['doc_sum'] = $this->request->get['ds'];
        // расшифровка типа дока в 1с-ный вид
        $doc_info['doc_type_1c'] = $this->model_account_one_si->decodeDocType($doc_info['doc_type']);
        

        $doc = $this->model_account_one_si->getDocument($doc_info);
        
        $data['is_nakl'] = (bool)($doc_info['doc_type'] == 'rtu');

        $data['doc_header'] = $doc_info;
        if (array_key_exists('Products', $doc)) {
            $data['doc_strings'] = $doc['Products'];
        }
        else {
            $data['doc_strings'] = array();
        }
        
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_account'),
            'href' => $this->url->link('account/account', '', true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('account/settlements')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('doc_title'),
            'href' => $this->url->link('account/settlements/doc')
        );

        $data['heading_title'] = $this->language->get('heading_title');
        $data['button_continue'] = $this->language->get('button_continue');
        $data['continue'] = $_SERVER['HTTP_REFERER'];

        $data['form_url'] = $this->url->link('account/settlements', '', true);
        $data['doc_url'] = $this->url->link('account/settlements/doc', '', true);

        $data['column_right'] = $this->load->controller('common/column_right');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');
        
        $data['lang'] = $this->language;

        $this->response->setOutput($this->load->view('account/settlements_doc', $data));
    }

}