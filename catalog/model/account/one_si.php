<?php
class ModelAccountOneSi extends Model
    // one si - 1с на английском
{
    private $connect_1c_url;
    private $stub;
    private $price_code;
    
    // хранить только общий корень 1с, остальное в запросах
    // общие функции для транслитерации ключей и преобр. чисел
    // убрать из функций префикс get
    
    
    function __construct($registry) {
        parent::__construct($registry);
        
        $this->load->library('socurl/curl');
        $this->load->library('sotranslit/translit');
        
        // заглушка для имитации работы с 1с
        $this->load->library('sostub/stub1c');
        $this->stub = new sostub\Stub1c;
        
        $this->price_code = '000000062';

//        $this->connect_1c_url = 'http://192.168.0.112/SinglOil';
//        $this->connect_1c_url = 'http://212.115.238.156:3352/SinglOil';
        $this->connect_1c_url = 'https://tech.singleoil.dp.ua/DEMO';
    }
    
    public function testConnect() {
        $this->load->library('socurl/curl');
        $curl = new socurl\Curl;
        
        $connect_str = 'http://google.com';
        
        $res = $curl->check_url($connect_str);
        
        if (!$res) {
            echo 'URL not found!';
        }
        else {
            echo 'URL OK!';
        }
    }
    
    public function testLib() {
        /* $this->load->library('socurl/curl');
        $curl = new socurl\Curl;
        debug($curl);
        
        $this->load->library('sotranslit/translit');
        $trl = new sotranslit\Translit;
        debug($trl);
        $cyr = 'fiva';
        debug($trl->transliterateToCyr($cyr)); */
    }
    
    public function prepare1cUser($user_data)
    {
        $trl = new sotranslit\Translit;
        
        $res = array();
        foreach ($user_data as $key => $row) {
            $res[$trl->transliterate($key)] = $row;
        }

        return $res;
    }
    
    public function get1cUserStub($login)
    {
        $res = $this->stub->get1cUser();
        return $res;
    }

    public function get1cUser($login)
    {
        // здесь авторизация через стаб настраивается в контроллере, сложная логика
//        if ($this->config->get('one_si_stub')) {
//            return $this->get1cUserStub($login);
//        }
        
        $connect_string = $this->connect_1c_url . '/hs/catalog/Users/Getuserinfo?email=<login>';
        $connect_string = str_replace('<login>', $login, $connect_string);

        $invalid_1 = 'Email not set or invalid format';
        $invalid_2 = 'User not found';
        $curl = new socurl\Curl;
        
        $response = $curl->get($connect_string);
        
        if ($response->body == $invalid_1 || $response->body == $invalid_2) {
            $res = array();
        }
        else {
            $res = json_decode($response->body);
            $res = $this->prepare1cUser($res);
        }
        
        return $res;
    }
    
    public function add1сCustomer($email, $name_1c, $code_1c, $password)
    {
        $customer = new stdClass();
        $customer->customer_group_id = 1;
        $customer->store_id = 0;
        $customer->language_id = 1;
        $customer->firstname = '';
        $customer->lastname = $name_1c;
        $customer->email = $email;
        $customer->code = $code_1c;
        $customer->salt = token(9);
        $customer->password = sha1($customer->salt . sha1($customer->salt . sha1($password)));
        $customer->status = 1;
        $customer->approved = 1;
        $customer->date_added = date('Y-m-d H:i:s');

        $this->db->query("
                INSERT INTO
                " . DB_PREFIX . "customer (
                    `customer_group_id`,
                    `store_id`,
                    `language_id`,
                    `firstname`,
                    `lastname`,
                    `email`,
                    `code`,
                    `password`,
                    `salt`,
                    `status`,
                    `approved`,
                    `date_added`
                )
                VALUES
                (
                    '" . $customer->customer_group_id . "',
                    '" . $customer->store_id . "',
                    '" . $customer->language_id . "',
                    '" . $customer->firstname . "',
                    '" . $customer->lastname . "',
                    '" . $customer->email . "',
                    '" . $customer->code . "',
                    '" . $customer->password . "',
                    '" . $customer->salt . "',
                    '" . $customer->status . "',
                    '" . $customer->approved . "',
                    '" . $customer->date_added . "'
                )
             ");

    }
    // end of customer.php
    
    private function get1cSettlements($user_code, $date_begin, $date_end)
    {
        $connect_string = $this->connect_1c_url . '/hs/report/MutualSettlementst/<user_code>/GetInfo';
        $connect_string = str_replace('<user_code>', $user_code, $connect_string);
        
        $curl = new socurl\Curl;
        $response = $curl->get($connect_string, array(
            'begin' => $date_begin,
            'end' => $date_end
        ));

        $res_cyr = json_decode($response->body, true);
        return $res_cyr;
    }
    
    private function encodeDocType($doc_type)
    {
        // docName - имя документа, сейчас реализованы такие: 
        // rtu - реализация товаров и услуг, 
        // ppv, ppi - платежное поручение входящее и исходящее, 
        // pko, rko - приходный и расходный кассовый ордер 
        $doc_type = mb_strtolower($doc_type, 'UTF-8');
        $res = '';
        switch ($doc_type) {
            case 'платежное поручение входящее':
                $res = 'ppv';
                break;
            case 'платежное поручение исходящее':
                $res = 'ppi';
                break;
            case 'реализация товаров и услуг':
                $res = 'rtu';
                break;
            case 'приходный кассовый ордер':
                $res = 'pko';
                break;
            case 'расходный кассовый ордер':
                $res = 'rko';
                break;
        }
        
        return $res;
    }
    
    public function decodeDocType($doc_type)
    {
        // обратная функция, 1с-ный тип дока по коду обмена с 1с
        $res = '';
        switch ($doc_type) {
            case 'ppv':
                $res = 'Платежное поручение входящее';
                break;
            case 'ppi':
                $res = 'Платежное поручение исходящее';
                break;
            case 'rtu':
                $res = 'Реализация товаров и услуг';
                break;
            case 'pko':
                $res = 'Приходный кассовый ордер';
                break;
            case 'rko':
                $res = 'Расходный кассовый ордер';
                break;
        }
        
        return $res;
    }
    
    private function prepare1cSettlements($settl_data)
    {
        // преобразуем 1с-ный массив с кирилистическими полями в массив с транслитерированными ключами
        $trl = new sotranslit\Translit;
        $res = Array();
        for ($i1 = 0; $i1 < sizeof($settl_data); $i1++) {
            $row = $settl_data[$i1];
            foreach ($row as $key => $val) {
                $res[$i1][$trl->transliterate($key)] = $val;
            }
            // также шифр кода дока, нужен для 1с-запроса расшифровки по строкам
            $res[$i1]['DocTypeCode'] = $this->encodeDocType($res[$i1]['Dokument']);
        }
        
        return $res;
    }
    
    private function prepare1cAccInfo($info_data)
    {
        // преобразуем 1с-ный массив с кирилистическими полями в массив с транслитерированными ключами
        $trl = new sotranslit\Translit;
        $res = Array();
        foreach ($info_data as $key => $val) {
            $res[$trl->transliterate($key)] = $val;
        }

        // акции
        if (sizeof($res['Aktsii']) > 0) {
            $promos = array();
            foreach ($res['Aktsii'] as $promo_key => $promo) {
                foreach ($promo as $key => $val) {
                    $promos[$promo_key][$trl->transliterate($key)] = $val;
                }
            }
            $res['Aktsii'] = $promos;
        }
        
        // дни доставки
        $deliv_days = '';
        $postfix = ', ';
        if ($res['D1'] == 'Да') {
            $deliv_days .= 'Понедельник';
        }
        if ($res['D2'] == 'Да') {
            if (strlen($deliv_days) > 0) {
                $deliv_days .= $postfix;
            }
            $deliv_days .= 'Вторник';
        }
        if ($res['D3'] == 'Да') {
            if (strlen($deliv_days) > 0) {
                $deliv_days .= $postfix;
            }
            $deliv_days .= 'Среда';
        }
        if ($res['D4'] == 'Да') {
            if (strlen($deliv_days) > 0) {
                $deliv_days .= $postfix;
            }
            $deliv_days .= 'Четверг';
        }
        if ($res['D5'] == 'Да') {
            if (strlen($deliv_days) > 0) {
                $deliv_days .= $postfix;
            }
            $deliv_days .= 'Пятница';
        }
        if ($res['D6'] == 'Да') {
            if (strlen($deliv_days) > 0) {
                $deliv_days .= $postfix;
            }
            $deliv_days .= 'Суббота';
        }
        if ($res['D7'] == 'Да') {
            if (strlen($deliv_days) > 0) {
                $deliv_days .= $postfix;
            }
            $deliv_days .= 'Восресенье';
        }
        $res['DeliveryDays'] = $deliv_days;
        
        return $res;
    }
    
    public function getSettlementsStub($user_code, $date_begin, $date_end)
    {
        $res = $this->stub->getSettlements();
        return $res;
    }

    public function getSettlements($user_code, $date_begin, $date_end)
    {
        // у функции есть заглушка, проверям опцию в конфиге
        if ($this->config->get('one_si_stub')) {
            return $this->getSettlementsStub($user_code, $date_begin, $date_end);
        }
        
        $res = $this->get1cSettlements($user_code, $date_begin, $date_end);
        $res = $this->prepare1cSettlements($res);
        
        return $res;
    }

    public function getDocumentStub($doc_info)
    {
        return array();
    }

    public function getDocument($doc_info)
    {
        if ($this->config->get('one_si_stub')) {
            return $this->getDocumentStub($doc_info);
        }

        $connect_string = $this->connect_1c_url . '/hs/documents/<doc_type>/<doc_num>/<doc_date>/Getinfo';
        $connect_string = str_replace('<doc_type>', $doc_info['doc_type'], $connect_string);
        $connect_string = str_replace('<doc_num>', $doc_info['doc_num'], $connect_string);
        $connect_string = str_replace('<doc_date>', $doc_info['doc_date'], $connect_string);
        
        $curl = new socurl\Curl;
        $response = $curl->get($connect_string);
        $res = json_decode($response->body, true);
        
        return $res;
    }
    
    public function getAccountInfoStub($user_email)
    {
        $res = array();
        $res['Naimenovanie'] = 'Автотехальянс (ЛВ)';
        $res['OsnovnoyMenedzherPokupatelyaNaimenovanie'] = 'Лукьяненко';
        $res['DebetorskayaZadolzhenosty'] = '12855.12';
        $res['DeliveryDays'] = 'Понедельник, Среда';
        $res['Real'] = '1870.38';
        
        return $res;
    }
    
    public function getAccountInfo($user_email)
    {
        if ($this->config->get('one_si_stub')) {
            return $this->getAccountInfoStub($user_email);
        }

        $connect_string = $this->connect_1c_url . '/hs/catalog/Users/Getuserinfo?email=<user_email>';
        $curl_url = str_replace('<user_email>', $user_email, $connect_string);
        
        $curl = new socurl\Curl;
        $response = $curl->get($curl_url);
        $res = json_decode($response->body, true);
        $res = $this->prepare1cAccInfo($res);

        return $res;
    }
    
    public function prepareUserPrices($prices_data)
    {
        $trl = new sotranslit\Translit;
        
        $res = array();
        foreach ($prices_data as $row) {
            $res2 = array();
            $prod_code = '';
            foreach ($row as $key => $val) {
                $lat_key = $trl->transliterate($key);
                $res2[$lat_key] = $val;
                if ($lat_key == 'NomenklaturaKod') {
                    $prod_code = $val;
                }
            }
            // ключами массива цен делаем код товара
            $res[$prod_code] = $res2;
        }
        
        // форматируем цену 
        foreach ($res as $code => $prod) {
            $price = $prod['Tsena'];
            $client_price = $prod['TsenaKlient'];

            // разделитель десятичных точка
            $price = str_replace(',', '.', $price);
            $client_price = str_replace(',', '.', $client_price);
            
            // округляем до целых копеек
            $price = round((float)$price, 2);
            $client_price = round((float)$client_price, 2);

            // нули справа
            $price = number_format($price, 2, '.', ''); 
            $client_price = number_format($client_price, 2, '.', ''); 
            
            $res[$code]['Tsena'] = $price;
            $res[$code]['TsenaKlient'] = $client_price;
        }

        return $res;
    }
    
    public function getUserPrices($user_code, $prod_codes)
    {
        $connect_string = $this->connect_1c_url . '/hs/report/Price/<price_code>/<user_code>/GetInfo?codes=<prod_codes>';
        $connect_string = str_replace('<price_code>', $this->price_code, $connect_string);
        $connect_string = str_replace('<user_code>', $user_code, $connect_string);
        $connect_string = str_replace('<prod_codes>', $prod_codes, $connect_string);
        
        $curl = new socurl\Curl;
        $response = $curl->get($connect_string);
        $res = json_decode($response->body, true);
        
        return $res;
    }
    
    public function getUserPricesStub($user_code, $products)
    {
        return array('0' => '9999.00');
    }
    
    public function setUserPrices($data, $user_code, $col_code, $col_price)
    {
        // используем штрих-код (ean) как код товара
        $prod_codes = '';
        foreach ($data as $prod) {
            $prod_codes .= $prod[$col_code] . ';';
        }

        // у функции есть заглушка, проверям опцию в конфиге
        if ($this->config->get('one_si_stub')) {
            $cl_prices = $this->getUserPricesStub($user_code, $prod_codes);
        }
        else {
            $cl_prices = $this->getUserPrices($user_code, $prod_codes);
        }
        
        $cl_prices = $this->prepareUserPrices($cl_prices);
        
        foreach ($data as $key => $prod) {
            $prod_code = $data[$key][$col_code];
            // если цена не назначена, 1с вместо 0 вообще не отдает данные по этому товару
            if (array_key_exists($prod_code, $cl_prices)) {
                $data[$key][$col_price] = $cl_prices[$prod_code]['TsenaKlient'];
            }
            else {
                $data[$key][$col_price] = 0;
            }
        }
        
        return $data;
    }
    
    public function setUserPrice($data, $user_code, $col_code, $col_price)
    {
        $cl_prices = $this->getUserPrices($user_code, $data[$col_code]);
        $cl_prices = $this->prepareUserPrices($cl_prices);
        
        $prod_code = $data[$col_code];
        if (array_key_exists($prod_code, $cl_prices)) {
            $data[$col_price] = $cl_prices[$prod_code]['TsenaKlient'];
        }
        else {
            $data[$col_price] = 0;
        }
        
        return $data;
    }
    
    public function add1cOrder($data)
    {
        $connect_string = $this->connect_1c_url . '/hs/documents/cos/<user_code>/PostOrder';
        $connect_string = str_replace('<user_code>', $data['customer_code'], $connect_string);
        
        $curl = new socurl\Curl;

        $params = array(
            'Products' => array()
        );
        if (isset($data['products'])) {
            foreach ($data['products'] as $prod) {
                $params['Products'][] = array(
                    'ProductCode' => $prod['ean'],
                    'Price' => $prod['price'], 
                    'Quantity' => $prod['quantity']
                );
            }
        }

        $params_json = json_encode($params);
        $response = $curl->post($connect_string, $params_json);

        $res = json_decode($response->body, true);        
        return $res;
    }
    
    public function get1cOrders($user_code)
    {
        $connect_string = $this->connect_1c_url . '/hs/documents/cos/<user_code>/Getinfo?startDate=<date_begin>';
        $connect_string = str_replace('<user_code>', $user_code, $connect_string);
        
        // за 2 последних недели
        $date_begin =  date("Ymd", strtotime("-14 days"));
        $connect_string = str_replace('<date_begin>', $date_begin, $connect_string);
        
        $curl = new socurl\Curl;
        $response = $curl->get($connect_string);
        $res = json_decode($response->body, true);

        return $res;
    }
    
    public function set1cOrdersStatus($orders, $user_code)
    {
        $orders_1c = $this->get1cOrders($user_code);
        $ord_stat = array();
        foreach ($orders_1c as $order) {
            $curr_stat = 'Ожидание';
            if ($order['Delete']) {
                $curr_stat = 'Отменено';
            }
            else if ($order['Conducted']) {
                $curr_stat = 'Сделка завершена';
            }
            else if ($order['ManualUpdate']) {
                $curr_stat = 'В обработке';
            }
            $ord_stat[$order['Number']] = $curr_stat;
        }
        
        foreach ($orders as $key => $order) {
            $ord_num = $order['invoice_prefix'];
            if (array_key_exists($ord_num, $ord_stat)) {
                $orders[$key]['status'] = $ord_stat[$ord_num];
            }
        }

        return $orders;
    }
   
    private function get1cReal($user_code)
    {
        $connect_string = $this->connect_1c_url . '/hs/report/Commission/<user_code>/GetInfo';
        $connect_string = str_replace('<user_code>', $user_code, $connect_string);
        
        $curl = new socurl\Curl;
        $response = $curl->get($connect_string);
        $res = json_decode($response->body, true);

        return $res;
    }
    
    public function prepareReal($real)
    {
        // транслитерация
        $trl = new sotranslit\Translit;
        $res = Array();
        for ($i1 = 0; $i1 < sizeof($real); $i1++) {
            $doc = $real[$i1];
            foreach ($doc as $key => $val) {
                $res[$i1][$trl->transliterate($key)] = $val;
            }
        }
        
        // форматируем цену 
        foreach ($res as $key => $doc) {
            $sum = $doc['SummaVzaimoraschetovOstatok'];
            $count = $doc['KolichestvoOstatok'];
            $price = 0;

            // разделитель десятичных точка
            $sum = str_replace(',', '.', $sum);
            // округляем до целых копеек
            $sum = round((float)$sum, 2);
            // цена = сумма / количество
            if ((int)$count > 0) {
                $price = (float)$sum / (int)$count;
                $price = round((float)$price, 2);
            }
            // добавляем нули справа
            $sum = number_format($sum, 2); 
            $price = number_format($price, 2); 
            
            $res[$key]['Sum'] = $sum;
            $res[$key]['Price'] = $price;
        }
        
        return $res;
    }
    
    public function getReal($user_code)
    {
        $real = $this->get1cReal($user_code);
        $real = $this->prepareReal($real);
        
        return $real;
    }
    
    public function getRealSum($real)
    {
        $res = 0;
        foreach ($real as $doc) {
            $res += $doc['Sum'];
        }
        
        return $res;
    }
    
}


// строки api к 1с:

// инфо пользователя, также используется для логина 
// '/hs/catalog/Users/Getuserinfo?email=<login>'

// взаиморасчеты
// '/hs/report/MutualSettlementst/<user_code>/GetInfo'

// информация по отдельному документу
// '/hs/documents/<doc_type>/<doc_num>/<doc_date>/Getinfo'

// цена клиента
// '/hs/report/Price/000000062/000001494/GetInfo?codes=000012048;000012180'

// товары на комиссии 
// '/hs/report/Commission/000001541/GetInfo'

// статусы заказов 
// /hs/documents/cos/000003686/Getinfo?startDate=20180101

// создать заказ
// /hs/documents/cos/000003686/PostOrder
/*это запрос на создание заказа, запрос конечно в режиме POST
в теле запроса объект типа 
{
"Products": [
{"ProductCode":"000011109", "Price":10, "Quantity":2},
{"ProductCode":"000011108", "Price":11, "Quantity":4}
]
}*/