function _cl(txt) {
    console.log(txt);
}

(function ($, undefined) {
    $(function () {
//        _cl($('.btn-navbar'));
        var btn_menu = $('.btn-navbar');
        var ctg_items = $('.soil-ctg-menu');
        var all_ctg_item = $('.soil-all-ctg-menu');
        var is_auth = $('#soil-is-auth').text();
        _cl(is_auth == 'auth');
        if (is_auth == 'auth') {
            if (btn_menu.is(":visible")) {
                ctg_items.show();
                all_ctg_item.hide();
            }
        }
        else {
            ctg_items.hide();
            all_ctg_item.hide();
        }
    });
})(jQuery);
