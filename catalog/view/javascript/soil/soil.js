function _cl(txt) {
    console.log(txt);
}

(function ($, undefined) {
    $(function () {
        console.log('SOIL');
        $('.input-group.date')
            .datetimepicker({
                locale: 'ru'
            })
            .on('dp.change', function (e) {
                if (!e.oldDate || !e.date.isSame(e.oldDate, 'day')) {
                    $(this).data('DateTimePicker').hide();
                }
            });

    });
})(jQuery);
