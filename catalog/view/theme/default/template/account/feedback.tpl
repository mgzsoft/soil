<?php echo $header; ?>

<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
        <div class="panel-body so-act-panel-body">
            <h4>
                Ваше запрос отправлен. С Вами свяжутся в ближайшее время.
            </h4>
        </div>
      <div class="buttons clearfix">
        <div class=""><a href="<?php echo $continue; ?>" class="btn btn-primary">
                <?php echo $button_continue; ?>
        </a></div>
      </div>
    </div>
    
    <?php echo $column_right; ?></div>
</div>

<?php echo $footer; ?> 