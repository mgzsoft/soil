<?php
    function tbsNum($num)
    {
        // преобразуем 1с-ный формат числа с разделителем-запятой в стандартный
        $num = (float)str_replace(',', '.', $num);

        $num = ((float)$num > 0) ? number_format((float)$num,  2, '.', '') : '';
        return $num;
    }
    
    function tbsDate($date)
    {
        return date_format(date_create($date), 'd.m.Y');
    }
?>

<?php echo $header; ?>
<div class="container">
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li>
            <a href="<?php echo $breadcrumb['href']; ?>">
                <?php echo $breadcrumb[ 'text']; ?> </a>
        </li>
        <?php } ?> </ul>
    <div class="row">
        <?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class='col-sm-6' ; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class='col-sm-9' ; ?>
        <?php } else { ?>
        <?php $class='col-sm-12' ; ?>
        <?php } ?>
        <div id="content" class=<?php echo $class; ?>>
            <?php echo $content_top; ?>
            <div class="col-sm-12">
                <h2>
                    <?= $doc_header['doc_num'] ?>
                </h2>
                <table class="table table-bordered table-hover so-std-table">
                    <thead>
                        <tr>
                            <td colspan="2" class="text-left">Детали документа</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="text-left" width="30%">
                                <b>Вид док-та:</b>
                            </td>
                            <td class="text-left">              
                                <?= $doc_header['doc_type_1c'] ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-left">
                                <b>№:</b>
                            </td>
                            <td class="text-left">              
                                <?= $doc_header['doc_num'] ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-left" width="30%">
                                <b>от:</b>
                            </td>
                            <td class="text-left">
                                <?= tbsDate($doc_header['doc_date']) ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-left" width="30%">
                                <b>Cумма:</b>
                            </td>
                            <td class="text-left">
                                <?= tbsNum($doc_header['doc_sum']) ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <?php if(!empty($doc_strings)): ?>
                <table class="table table-bordered table-hover so-st-table">
                    <thead>
                        <tr>
                            <td class="text-center">Код</td>
                            <td class="text-center">Товар</td>
                            <td class="text-center">К-во</td>
                            <td class="text-center">Цена</td>
                            <td class="text-center">Сумма</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($doc_strings as $str): ?>
                        <tr>
                            <td class="text-left">
                                <?= $str['ProductCode'] ?>
                            </td>
                            <td class="text-left">
                                <?= $str['ProductName'] ?>
                            </td>
                            <td class="text-right">
                                <?= $str['Quantity'] ?>
                            </td>
                            <td class="text-right">
                                <?= tbsNum($str['Price']) ?>
                            </td>
                            <td class="text-right">
                                <?= tbsNum((float)$str['Quantity']*(float)$str['Price']) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                        <tr>
                            <td colspan="4" class="text-right"><b>Итого:</b></td>
                            <td class="text-right">
                                <b><?= tbsNum($doc_header['doc_sum']) ?></b>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <?php endif; ?>
                
                <div class="buttons clearfix">
                    <div class="pull-right">
                        <a href="<?= $continue ?>" class="btn btn-primary">
                            <?= $button_continue ?>
                        </a>
                    </div>
                </div>
            </div>
        </div> 
        <?php echo $content_bottom; ?> </div>
        <?php echo $column_right; ?> </div>
</div>
<?php echo $footer; ?>
