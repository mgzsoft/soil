<?php
    function tbsNum($num)
    {
        // преобразуем 1с-ный формат числа с разделителем-запятой в стандартный
        $num = (float)str_replace(',', '.', $num);

        $num = ((float)$num > 0) ? number_format((float)$num,  2, '.', '') : '';
        return $num;
    }
    
    function tbsDate($date)
    {
        return date_format(date_create($date), 'd.m.Y');
    }
?>

<?php echo $header; ?>
<div class="container">
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li>
            <a href="<?php echo $breadcrumb['href']; ?>">
                <?php echo $breadcrumb[ 'text']; ?> </a>
        </li>
        <?php } ?> </ul>
    <div class="row">
        <?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class='col-sm-6' ; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class='col-sm-9' ; ?>
        <?php } else { ?>
        <?php $class='col-sm-12' ; ?>
        <?php } ?>
        <div id="content" class=<?php echo $class; ?>>
            <?php echo $content_top; ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-bar-chart"></i>&nbsp; Взаиморасчеты</h3> </div>
                <div class="panel-body">
                    <form action="<?php echo $form_url ?>" method="get" enctype="multipart/form-data">
                        <input type="hidden" name="route" value="account/settlements">
                    <div class="well">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for="input-date-start">Дата начала:</label>
                                    <div class="input-group date">
                                        <input type="text" name="filter_date_start" value="<?= $filter_date_start ?>" placeholder="Дата начала:" data-date-format="DD-MM-YYYY" id="input-date-start" class="form-control"> <span class="input-group-btn">
                                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for="input-date-end">Дата окончания:</label>
                                    <div class="input-group date">
                                        <input type="text" name="filter_date_end" value="<?= $filter_date_end ?>" placeholder="Дата окончания:" data-date-format="DD-MM-YYYY" id="input-date-end" class="form-control"> <span class="input-group-btn">
                                        <button type="button" class="btn btn-default">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </div>
                                </div>
                                <button type="submit" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-calendar-check-o"></i>&nbsp; Сформировать</button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
            
            <div>
            <?php if ($report_status == 'not_runned'): ?>
                <!-- <table class="table table-bordered table-hover so-st-table">
                    <thead>
                        <tr>
                            <td class="text-center">Документ</td>
                            <td class="text-center">№</td>
                            <td class="text-center">Дата</td>
                            <td class="text-center">Приход</td>
                            <td class="text-center">Расход</td>
                            <td class="text-center">Сальдо</td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="5" class="text-right"><b>Сальдо на конец:</b></td>
                            <td class="text-right">
                                <b><?= $balance_end ?></b>
                            </td>
                            <td></td>
                        </tr>
                    </tbody>
                </table> -->
            <?php elseif($report_status == 'incorrect_date'): ?>
                <h4>
                    Неправильно выбран диапазон дат!
                </h4>
            <?php elseif($report_status == 'no_result'): ?>
                <h4>
                    За данный период оборотов нет.
                </h4>
            <?php elseif($report_status == 'ok'): ?>
                
                <table class="table table-bordered table-hover so-st-table">
                    <thead>
                        <tr>
                            <td class="text-center">Документ</td>
                            <td class="text-center">№</td>
                            <td class="text-center">Дата</td>
                            <td class="text-center">Приход</td>
                            <td class="text-center">Расход</td>
                            <td class="text-center">Сальдо</td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="5" class="text-right"><b>Сальдо на начало:</b></td>
                            <td class="text-right">
                                <b><?= tbsNum($balance_begin) ?></b>
                            </td>
                            <td></td>
                        </tr>
                        <?php foreach($docs as $doc): ?>
                            <tr>
                                <td class="text-left">
                                    <?= $doc['Dokument'] ?>
                                </td>
                                <td class="text-left">
                                    <?= $doc['DokumentNomer'] ?>
                                </td>
                                <td class="text-right">
                                    <?= tbsDate($doc['DokumentData']) ?>
                                </td>
                                <td class="text-right">
                                    <?= tbsNum($doc['SummaVzaimoraschetovPrihod']) ?>
                                </td>
                                <td class="text-right">
                                    <?= tbsNum($doc['SummaVzaimoraschetovRashod']) ?>
                                </td>
                                <td class="text-right">
                                    <?= tbsNum($doc['SummaVzaimoraschetovKonechniyOstatok']) ?>
                                </td>
                                <td class="text-center">
                                    <a href="<?= $doc['url'] ?>" data-toggle="tooltip" title="" class="btn-sm btn-light" data-original-title="Просмотр">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        <tr>
                            <td colspan="5" class="text-right"><b>Сальдо на конец:</b></td>
                            <td class="text-right">
                                <b><?= tbsNum($balance_end) ?></b>
                            </td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            <? else: ?>
                <!-- no_result, отчет сформировали, но движений не было -->
                <!-- восстановить ветку после доработки 1с по выдаче сальдо на дату -->
                <!-- <table class="table table-bordered table-hover so-st-table">
                        <thead>
                            <tr>
                                <td class="text-center">Документ</td>
                                <td class="text-center">№</td>
                                <td class="text-center">Дата</td>
                                <td class="text-center">Приход</td>
                                <td class="text-center">Расход</td>
                                <td class="text-center">Сальдо</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="5" class="text-right"><b>Сальдо на начало:</b></td>
                                <td class="text-right">
                                    <b><?= $balance_begin ?></b>
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="6" class="text-right">За данный период оборотов нет.</td>
                                <td class="text-right"></td>
                            </tr>
                            <tr>
                                <td colspan="5" class="text-right"><b>Сальдо на конец:</b></td>
                                <td class="text-right">
                                    <b><?= $balance_end ?></b>
                                </td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table> -->
            <?php endif; ?>

            <div class="buttons clearfix">
                <div class="pull-right">
                    <a href="<?= $continue ?>" class="btn btn-primary">
                        <?= $button_continue ?>
                    </a>
                </div>
            </div>
        </div> 
        <?php echo $content_bottom; ?> </div>
        <?php echo $column_right; ?> </div>
</div>
<?php echo $footer; ?>