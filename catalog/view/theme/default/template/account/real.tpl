<?php echo $header; ?>
<div class="container">
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
    </ul>
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
            <?php $class = 'col-sm-6'; ?>
        <?php }
        elseif ($column_left || $column_right) { ?>
            <?php $class = 'col-sm-9'; ?>
        <?php }
        else { ?>
    <?php $class = 'col-sm-12'; ?>
<?php } ?>
        <div id="content" class=<?php echo $class; ?>><?php echo $content_top; ?>
            
        <div class="col-sm-12">
            <h2><?php echo $heading_title; ?></h2>
            <h4 style="margin-bottom: 24px;">товары на реализации на текущую дату</h4>
                
                <table class="table table-bordered table-hover so-st-table">
                    <thead>
                        <tr>
                            <td class="text-center">Код</td>
                            <td class="text-center">Товар</td>
                            <td class="text-center">К-во</td>
                            <td class="text-center">Баз. цена</td>
                            <td class="text-center">Сумма</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($real as $doc): ?>
                            <tr>
                                <td class="text-left">
                                    <?= $doc['NomenklaturaKod'] ?>
                                </td>
                                <td class="text-left">
                                    <?= $doc['NomenklaturaNaimenovanie'] ?>
                                </td>
                                <td class="text-right">
                                    <?= $doc['KolichestvoOstatok'] ?>
                                </td>
                                <td class="text-right">
                                    <?= $doc['Price'] ?>
                                </td>
                                <td class="text-right">
                                    <?= $doc['Sum'] ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>

                            <td colspan="4" class="text-right"><b>Общая стоимость товаров на реализации:</b></td>
                            <td class="text-right">
                                <b><?= $real_sum ?></b>
                            </td>
                        </tr>
                    </tbody>
                </table>
                                
                <div class="buttons clearfix">
                    <div class="pull-right">
                        <a href="<?= $continue ?>" class="btn btn-primary">
                            Продолжить
                        </a>
                    </div>
                </div>
            </div>            
        </div>        

<?php echo $content_bottom; ?></div>
<?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
