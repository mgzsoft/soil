<?php
    function tbsNum($num)
    {
        // преобразуем 1с-ный формат числа с разделителем-запятой в стандартный
        $num = (float)str_replace(',', '.', $num);

        $num = ((float)$num > 0) ? number_format((float)$num,  2, '.', '') : '';
        return $num;
    }
    
    function tbsDate($date)
    {
        return date_format(date_create($date), 'd.m.Y');
    }
?>

<?php echo $header; ?>

<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-user"></i>
                    &nbsp; <?= $user['Naimenovanie'] ?>
                </h3>
            </div>
            <div class="panel-body so-act-panel-body">
                <table class="table table-bordered table-hover so-act-table">
                    <tbody>
                        <tr>
                            <td class="text-left so-act-left-col" width="30%">
                                <b>Ваш ведущий менеджер:</b>
                            </td>
                            <td class="text-left">              
                                <?= $user['OsnovnoyMenedzherPokupatelyaNaimenovanie'] ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-left so-act-left-col">
                                <b>Текущее сальдо:</b>
                            </td>
                            <td class="text-left">              
                                <?= tbsNum($user['DebetorskayaZadolzhenosty']) ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-left so-act-left-col">
                                <b>Доставка (дни недели):</b>
                            </td>
                            <td class="text-left">              
                                <?= $user['DeliveryDays'] ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-left so-act-left-col">
                                <b>Сумма товара на реализации:</b>
                            </td>
                            <td class="text-left">              
                                <?= $user['KomissiyaSumma'] ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <?php if (sizeof($user['Aktsii']) > 0): ?>
            <?php $promo_header = (sizeof($user['Aktsii']) == 1 ? 'Текущая акция' : 'Текущие акции'); ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="fa fa-shopping-cart"></i>
                        &nbsp; <?= $promo_header ?>
                    </h3>
                </div>
                <div class="panel-body">
                    <?php for ($i1 = 0; $i1 < sizeof($user['Aktsii']); $i1++): ?>
                        <?php if ($i1 > 0): ?>
                            <hr>
                        <?php endif; ?>
                        <h3>
                            <?= $user['Aktsii'][$i1]['AktsiyaOpisanie'] ?>
                        </h3>
                    <?php endfor; ?>
                </div>
            </div>
        <?php endif; ?>
        
        <form action="<?= $feedback_action ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
            <button type="submit" class="btn btn-light pull-left so-act-call-btn"><i class="fa fa-phone"></i>
                &nbsp; Перезвоните мне
            </button>
        </form>
    </div>
    
    <?php echo $column_right; ?></div>
</div>

<?php echo $footer; ?> 
