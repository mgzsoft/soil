<?php
    
namespace sotranslit;
    
class Translit {
    private $cyr;
    private $lat;
     
    function __construct() {
        $this->cyr = [
            'а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п',
            'р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я',
            'А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П',
            'Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я'
        ];
        $this->lat = [
            'a','b','v','g','d','e','io','zh','z','i','y','k','l','m','n','o','p',
            'r','s','t','u','f','h','ts','ch','sh','sht','a','i','y','e','yu','ya',
            'A','B','V','G','D','E','Io','Zh','Z','I','Y','K','L','M','N','O','P',
            'R','S','T','U','F','H','Ts','Ch','Sh','Sht','A','I','Y','e','Yu','Ya'
        ];
    }

    public function transliterate($txt)
    {
        $res = str_replace($this->cyr, $this->lat, $txt);
        return $res;
    }

    public function transliterateToCyr($txt)
    {
        $res = str_replace($this->lat, $this->cyr, $txt);  
        return $res;
    }
}