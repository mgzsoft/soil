<?php
    
namespace sostub;
    
class Stub1c {
    public function getSettlements()
    {
        $dir = dirname(__FILE__);
        $res = file($dir.'/settlements.txt');
        $res = trim((string)$res[0]);
        $res = unserialize($res);
        
        return $res;
    }
    
    public function get1cUser()
    {
        $dir = dirname(__FILE__);
        $res = file($dir.'/login.txt');
        $res = trim((string)$res[0]);
        $res = unserialize($res);
        
        return $res;
    }
}